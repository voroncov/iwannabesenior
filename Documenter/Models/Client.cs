﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Documenter.Models
{
    public class Client
    {
        private IList<Document> _documents;
        public virtual Guid Id { get; set; }
        public virtual string Username { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual IList<Document> Documents {
            get
            {
                return _documents ?? (_documents = new List<Document>());
            }
            set
            {
                _documents = value;
            }
        }

    }
}