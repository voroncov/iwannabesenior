﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Documenter.Models
{
    public class Document
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string RealName { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual Client Autor { get; set; }
        public virtual byte[] Data { get; set; }
    }
}