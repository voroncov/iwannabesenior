﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Documenter.Models.Helpers
{
    public class DocumentAdd
    {
        [DisplayName("Имя документа")]
        [Required(AllowEmptyStrings =false, ErrorMessage ="Требуется имя документа!")]
        [StringLength(255, MinimumLength =1, ErrorMessage = "Требуется назание от 1 до 255 символов!")]
        
        public string Name { get; set; }
        [DisplayName("Файл")]
        [Required(ErrorMessage ="Требуется файл!")]
        public string Upload_file { get; set; }
    }
}