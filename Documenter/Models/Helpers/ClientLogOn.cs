﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Documenter.Models.Helpers
{
    public class ClientLogOn
    {
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Требуется ввести от 1 до 255 символов!")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Требуется логин!")]
        [DisplayName("Логин")]
        public string Username { get; set; }
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Требуется ввести от 1 до 255 символов!")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Требуется пароль!")]
        [DisplayName("Пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}