﻿using Documenter.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Documenter.Controllers
{
    public class AuthController : Controller
    {
        public ActionResult LogOn()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOn(Documenter.Models.Helpers.ClientLogOn model, string returnUrl)
        {
            string decodedUrl = "";
            if (!string.IsNullOrWhiteSpace(returnUrl))
                decodedUrl = Server.UrlDecode(returnUrl);
            if (ModelState.IsValid)
            {
                var repository = new ClientRepository();
                var client = repository.GetByUserName(model.Username)
                    .FirstOrDefault(it => model.Password == it.PasswordHash);
                if (client != null)
                {
                    FormsAuthentication.SetAuthCookie(client.Id.ToString(), true);
                    if (Url.IsLocalUrl(decodedUrl))
                    {
                        return Redirect(decodedUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                ModelState.AddModelError("", "Неверный логин/пароль");
            }
            else
            {
                ModelState.AddModelError("", "Некорректное заполнение полей!");
            }
            return View();
        }

        public ActionResult LogOut(string returnUrl)
        {
            string decodedUrl = "";
            if (!string.IsNullOrWhiteSpace(returnUrl))
                decodedUrl = Server.UrlDecode(returnUrl);
            FormsAuthentication.SignOut();
            if (Url.IsLocalUrl(decodedUrl))
            {
                return Redirect(decodedUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}