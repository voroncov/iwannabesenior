﻿using Documenter.Models;
using Documenter.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Documenter.Controllers
{
    [Authorize]
    public class DocumentsController : Controller
    {
        public ActionResult Index()
        {
            var documentRepository = new DocumentRepository();
            var documents = new List<Document>();
            ViewBag.Documents = documents.Concat(documentRepository.GetOrderedBy("Name"));
            return View();
        }
        public ActionResult Add()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Add(string name, HttpPostedFileBase Upload_file)
        {
            if (Upload_file != null)
            {
                var realName = Path.GetFileName(Upload_file.FileName);
                if(!Directory.Exists(Server.MapPath("~/Files/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Files/"));
                }
                if (ModelState.IsValid)
                {
                    var clientRepository = new ClientRepository();
                    var client = clientRepository.GetByGuid(Guid.Parse(User.Identity.Name));
                    var document = new Document()
                    {
                        Id = new Guid(),
                        Name = name,
                        RealName = realName,
                        Autor = client,
                        Date = DateTime.Now,
                        Data = new byte[Upload_file.InputStream.Length]
                    };

                    var documentRepository = new DocumentRepository();
                    documentRepository.Add(document);

                    Upload_file.InputStream.Read(document.Data, 0, (int)Upload_file.InputStream.Length);
                    Upload_file.SaveAs(Server.MapPath($"~/Files/{document.Id.ToString()}"));
                }
                else
                {
                    ModelState.AddModelError("", "Неверное заполнение полей!");
                }

                ViewBag.FileStatus = "Файл успешно загружен!";
            }
            return RedirectToAction("Index");
            //return View("Index");
        }
        public ActionResult All()
        {
            return RedirectToAction("Index");
        }

        public ActionResult Order(string orderby)
        {
            var documentRepository = new DocumentRepository();
            return PartialView(documentRepository.GetOrderedBy(orderby));
        }

        public FileResult Download(string file_guid)
        {
            var documentRepository = new DocumentRepository();
            var document = documentRepository.GetByGuid(Guid.Parse(file_guid));
            if (document != null)
            {
                if (!System.IO.File.Exists(Server.MapPath("~/Files/") + document.Id.ToString()))
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/") + document.Id.ToString(), document.Data);
                }
                return File(Server.MapPath("~/Files/") + document.Id.ToString(), "application/octet-stream", document.RealName);
            }
            return null;
        }
    }
}