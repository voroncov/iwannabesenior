﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Documenter
{
    public class NHibernater
    {
        public static ISession Session { get; private set; }

        public static void Open ()
        {
            if (Session != null && Session.IsOpen)
            {
                Session.Close();
            }
            var config = new Configuration()
                .DataBaseIntegration(dataBase =>
                {
                    dataBase.ConnectionString = $"Server=localhost;Initial Catalog=Documenter;Integrated Security=SSPI";
                    dataBase.Dialect<MsSql2012Dialect>();
                });
            var mapper = new ModelMapper();
            mapper.AddMappings(Assembly.GetExecutingAssembly().GetExportedTypes());
            var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            config.AddMapping(mapping);
            new SchemaUpdate(config).Execute(useStdOut: true, doUpdate: true);
            var sessionFactory = config.BuildSessionFactory();
            Session = sessionFactory.OpenSession();
        }
    }
}