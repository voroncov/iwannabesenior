﻿using Documenter.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Documenter.Mappings
{
    public class ClientMapping : ClassMapping<Client>
    {
        public ClientMapping()
        {
            Id(it => it.Id, map => map.Generator(Generators.Guid));
            Property(it => it.Username);
            Property(it => it.PasswordHash);
            Bag(it => it.Documents, collect =>
            {
                collect.Key(key => key.Column(""));
                collect.Inverse(true);
            },
            map => map.OneToMany());
        }
    }
}