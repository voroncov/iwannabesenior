﻿using Documenter.Models;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Documenter.Mappings
{
    public class DocumentMapping : ClassMapping<Document>
    {
        public DocumentMapping()
        {
            Id(it => it.Id, map => map.Generator(Generators.Guid));
            Property(it => it.Name);
            Property(it => it.RealName);
            Property(it => it.Date);
            Property(it => it.Data, map =>
            {
                map.Type(NHibernateUtil.BinaryBlob);
                map.Length(int.MaxValue);
            });
            ManyToOne(it => it.Autor, map =>
            {
                map.Cascade(Cascade.Persist);
                map.Column("AutorId");
            });
        }
    }
}