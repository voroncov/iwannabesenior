﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Documenter.Models;

namespace Documenter.Repositories
{
    public class DocumentRepository : IDocumentRepository
    {
        public void Add(Document document)
        {
            NHibernater.Open();
            using (var transaction = NHibernater.Session.BeginTransaction())
            {
                NHibernater.Session.Save(document);
                transaction.Commit();
            }
        }

        public Document GetByGuid(Guid guid)
        {
            Document document = null;
            NHibernater.Open();
            using (NHibernater.Session.Transaction)
            {
                var criteria = NHibernater.Session.CreateCriteria<Document>();
                document = criteria.List<Document>().FirstOrDefault(it => it.Id == guid);
            }
            return document;
        }

        public IEnumerable <Document> GetOrderedBy(string criterion)
        {
            NHibernater.Open();
            IEnumerable<Document> documents;
            using (NHibernater.Session.Transaction)
            {
                var criteria = NHibernater.Session.CreateCriteria<Document>();
                documents = criteria
                    .AddOrder(NHibernate.Criterion.Order.Asc(criterion ?? "Name"))
                    .List<Document>();
            }
            return documents;
        }
    }
}