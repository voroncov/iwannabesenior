﻿using Documenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documenter.Repositories
{
    interface IClientRepository
    {
        Client GetByGuid(Guid guid);
        IEnumerable<Client> GetByUserName(string name);
    }
}
