﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Documenter.Models;

namespace Documenter.Repositories
{
    public class ClientRepository:IClientRepository
    {
        public Client GetByGuid(Guid guid)
        {
            Client client = null;
            NHibernater.Open();
            using (NHibernater.Session.BeginTransaction())
            {
                var criteria = NHibernater.Session.CreateCriteria<Client>();
                client = criteria.List<Client>().FirstOrDefault(it => it.Id == guid);
            }
                return client;
        }

        public IEnumerable<Client> GetByUserName(string name)
        {
            IEnumerable <Client> clients = null;
            NHibernater.Open();
            using (NHibernater.Session.BeginTransaction())
            {
                var criteria = NHibernater.Session.CreateCriteria<Client>();
                clients = criteria.List<Client>().Where(it => it.Username == name);
            }
                return clients;
        }
    }
}