﻿using Documenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documenter.Repositories
{
    interface IDocumentRepository
    {
        void Add(Document document);
        Document GetByGuid(Guid guid);
        IEnumerable<Document> GetOrderedBy(string criterion);
    }
}
